import React from 'react';
import Link from 'gatsby-link';

const IndexPage = () =>
  (<div>
    <h1>Welcome!</h1>
    <p>
      <a href="http://forum.freeso.org/threads/is-it-difficult-to-learn-code-core-business-unit-information-and-tools-for-learning.2797/">FreeSO Forums - is it difficult to learn code (Core Business Unit)? - Information and tools for learning</a>
    </p>
    <p>
      <a href="https://dashboard.thecode.house">FreeSO Lot Notifier</a>
    </p>
    <div>
      Flowcharts and yelling letter combinations in property chat are so 2002. Find a team and use our bot to play code the modern way on <a href="https://discord.gg/te8mZ9y">The Code House{'\''}s Discord Server</a>.
      <div className="fine-print">
        NOTE: You must have a verified email address and wait 10 minutes before you can speak in the server.
      </div>
    </div>
    { /* <Link to="/page-2/">Go to page 2</Link> */ }
    <div className="fine-print footer">This site is not affiliated with or endorsed by Electronic Arts.</div>
  </div>);

export default IndexPage;
