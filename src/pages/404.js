import React from 'react'

const NotFoundPage = () =>
  <div>
    <h1>O Frazinnratt!</h1>
    <p>That page doesn't exist!</p>
  </div>

export default NotFoundPage
