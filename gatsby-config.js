module.exports = {
  siteMetadata: {
    title: `The Code House`,
  },
  plugins: [`gatsby-plugin-react-helmet`],
}
